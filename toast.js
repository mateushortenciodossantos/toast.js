function objectStyleMerger(customStyle, obj) {
  for (var property in customStyle) {
    if (obj.style.hasOwnProperty(property)) {
      obj.style[property] = customStyle[property];
    } else {
      obj.prototype[property] = customStyle[property];
    }
  }

  return obj;
}

var Toast = (function() {
  function Toast() {}
  Toast.prototype.show = function(text, customStyle) {
    var _toast = document.createElement('div');

    if (customStyle) {
      _toast = objectStyleMerger(customStyle, _toast);
    }

    var _h2Text = document.createElement('h2');
    _h2Text.innerText = text;
    _h2Text.setAttribute("id", "toastText");
    _toast.appendChild(_h2Text);
    _toast.setAttribute("id", "toast");
    _toast.style.opacity = 0;

    document.body.appendChild(_toast);

    var toastToInsert = document.getElementById("toast");

    setTimeout(function() {
      toastToInsert.style.opacity = 1;
    }, 100);

    setTimeout(function() {
      toastToInsert.style.opacity = 0;
    }, 3100);

    setTimeout(function() {
      toastToInsert.parentNode.removeChild(toastToInsert);
    }, 3600);
  };
  return Toast;
}());